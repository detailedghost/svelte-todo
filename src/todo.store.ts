import { writable } from "svelte/store";

interface ToDoItem {
  id: string;
  name: string;
  isDone: boolean;
}

export const todoListStore = writable<ToDoItem[]>([]);

export const addTodo = (item: ToDoItem) =>
  todoListStore.update((list: ToDoItem[]) => [...list, item]);

export const removeTodo = (item: ToDoItem) =>
  todoListStore.update((list) => list.filter((i) => i.id !== item.id));

export const updateToDo = (item: ToDoItem) =>
  todoListStore.update((list) =>
    list.map((li) => {
      if (li.id === item.id) return item;
      return li;
    })
  );
