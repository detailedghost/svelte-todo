import { defineConfig } from "vite";
import svelte from "@sveltejs/vite-plugin-svelte";

// https://vitejs.dev/config/
export default defineConfig({
  base: "/svelte-todo/",
  build: {
    outDir: "public",
  },
  publicDir: "static",
  plugins: [svelte()],
});
